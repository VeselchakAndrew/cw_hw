package homework2;

import java.util.Arrays;

public class ArrayTools {
    public static int[] invertFromMiddle(int[] arr) {
        int[] invertedArray = new int[arr.length];
        int middleIndex = arr.length / 2 + 1;
        for (int i = 0; i < middleIndex; i++) {
            invertedArray[i] = arr[arr.length - 1 - i];
            invertedArray[arr.length - 1 - i] = arr[i];
        }
        return invertedArray;
    }

    public static boolean isExist(int[] arr, int value) {
        boolean isExist = false;
        for (int j : arr) {
            if (j == value) {
                isExist = true;
                break;
            }
        }
        return isExist;
    }

    public static int[] insertArray(int[] arr, int[] values) {

        int[] combinedArray = new int[arr.length + values.length];
        System.arraycopy(arr, 0, combinedArray, 0, arr.length);
        System.arraycopy(values, 0, combinedArray, arr.length, values.length);
        Arrays.sort(combinedArray);


//
//        for (int i = 0; i < values.length; i++) {
//            int searchedItem = values[i];
//            int index = Arrays.binarySearch(combinedArray, searchedItem);
//            if (index < 0) {
//                index = -index - 1;
//            }
//            if (index >= combinedArray.length) {
//                index = combinedArray.length - 1;
//            }
//            int arrayElem = combinedArray[index];
//            if (arrayElem != 0) {
//                System.arraycopy(combinedArray, index, combinedArray, index + 1, combinedArray.length - 1 - index);
//            }
//            System.arraycopy(values, i, combinedArray, index, 1);
//        }
        return combinedArray;
    }

}
