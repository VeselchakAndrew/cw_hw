package homework2;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        int[] initialArr = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        int[] sortedArr = {1, 3, 5, 6, 7, 8};
        int[] arrayToInsert = {2, 4, 11, 9};
        System.out.println(Arrays.toString(ArrayTools.invertFromMiddle(initialArr)));
        System.out.println(ArrayTools.isExist(initialArr, 0));
        System.out.println(ArrayTools.isExist(initialArr, 4));
        System.out.println(Arrays.toString(ArrayTools.insertArray(sortedArr, arrayToInsert)));

    }
}
